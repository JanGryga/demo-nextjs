import Layout from '@/components/Layout';

export default function AboutPage() {
  return (
    <Layout title='About DJ Events'>
      <h1>About</h1>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium
        eius at iusto sed explicabo consequatur aperiam architecto itaque vero
        doloremque?
      </p>
      <p>Version: 1.0.1</p>
    </Layout>
  );
}
